//
//  YUCalendarMonthView.h
//  YUCalendarKeyboard
//
//  Created by Sikhapol Saijit on 1/27/16.
//  Copyright © 2016 Sikhapol Saijit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YUCalendarMonthView : UIView

@property (nonatomic) NSDateComponents *month;
@property (nonatomic) NSDateComponents *selectedDate;

@property (nonatomic) UILabel *label;

@end
