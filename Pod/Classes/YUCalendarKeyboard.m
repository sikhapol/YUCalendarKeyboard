//
//  YUCalendarKeyboard.m
//  YUCalendarKeyboard
//
//  Created by Sikhapol Saijit on 1/27/16.
//  Copyright © 2016 Sikhapol Saijit. All rights reserved.
//

#import "YUCalendarKeyboard.h"

#import "YUCalendarMonthView.h"

@interface YUCalendarKeyboard () <UIScrollViewDelegate>

@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) NSArray *monthViews;

@end

@implementation YUCalendarKeyboard

- (instancetype)init {
    return [super initWithFrame:CGRectMake(0, 0, 375, 200)];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.scrollView.frame = self.bounds;
    self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.scrollView.bounds) * 3, CGRectGetHeight(self.scrollView.bounds));
    
    for (NSInteger i = 0; i < self.monthViews.count; ++i) {
        UIView *view = self.monthViews[i];
        view.frame = CGRectMake(i * CGRectGetWidth(self.scrollView.bounds), 0, CGRectGetWidth(self.scrollView.bounds), CGRectGetHeight(self.scrollView.bounds));
        [self.scrollView addSubview:view];
    }
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    
    [self addSubview:(self.scrollView = self.scrollView ?: [[UIScrollView alloc] init])];
    self.scrollView.delegate = self;
    self.scrollView.scrollEnabled = YES;
    
    self.monthViews = @[[[YUCalendarMonthView alloc] init], [[YUCalendarMonthView alloc] init], [[YUCalendarMonthView alloc] init]];
}

#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageOffset = scrollView.contentOffset.x / CGRectGetWidth(scrollView.bounds);
    NSLog(@"%g", pageOffset);
    if (pageOffset < 0.5) {
        CGPoint newOffset = scrollView.contentOffset;
        newOffset.x += CGRectGetWidth(scrollView.bounds);
        scrollView.contentOffset = newOffset;
    } else if (pageOffset > 1.5) {
        CGPoint newOffset = scrollView.contentOffset;
        newOffset.x -= CGRectGetWidth(scrollView.bounds);
        scrollView.contentOffset = newOffset;
    }
}

@end
