//
//  YUCalendarMonthView.m
//  YUCalendarKeyboard
//
//  Created by Sikhapol Saijit on 1/27/16.
//  Copyright © 2016 Sikhapol Saijit. All rights reserved.
//

#import "YUCalendarMonthView.h"

@implementation YUCalendarMonthView

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    
    self.label = self.label ?: [[UILabel alloc] init];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self.label sizeToFit];
    
}

@end
