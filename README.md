# YUCalendarKeyboard

[![CI Status](http://img.shields.io/travis/Sikhapol Saijit/YUCalendarKeyboard.svg?style=flat)](https://travis-ci.org/Sikhapol Saijit/YUCalendarKeyboard)
[![Version](https://img.shields.io/cocoapods/v/YUCalendarKeyboard.svg?style=flat)](http://cocoapods.org/pods/YUCalendarKeyboard)
[![License](https://img.shields.io/cocoapods/l/YUCalendarKeyboard.svg?style=flat)](http://cocoapods.org/pods/YUCalendarKeyboard)
[![Platform](https://img.shields.io/cocoapods/p/YUCalendarKeyboard.svg?style=flat)](http://cocoapods.org/pods/YUCalendarKeyboard)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YUCalendarKeyboard is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "YUCalendarKeyboard"
```

## Author

Sikhapol Saijit, sikhapol@gmail.com

## License

YUCalendarKeyboard is available under the MIT license. See the LICENSE file for more info.
